**URI minter template**

this is a simple tamplete based off a [contract I deployed](https://opensea.io/assets/0x75bfe3a810a4051f29c004d09efb31d3c4988ade/0) that worked for my needs. Please understand fully what it does and what it doesn't do before deploying on mainnet. Feel free to reach out to @mimnermosart on twitter or mimnermos@mimnermos.art for questions.

What it does: the contract can grant a minter role to an address, which than is able to mint multiple tokens in the contract by specifing the receiving address and the token URI for metadata. Some manual effort is required to substitute names and whatnot.

What it doesn't do: anything else. Royalties, listing, etc. is all demanded to external platforms. What is tested at the time of writing is just a simple sale on opensea.

This project doesn't come with a website, so minting must be done through etherscan.io after having verified the source code (minting through code is possible I guess, but wasn't tested)

Steps to try it:

- clone this repository locally
- install truffle with `npm install -g truffle`
- run `npm install` to install dependencies
- change inside `contracts/URIMinter.sol` the contract name and ticker symbol
- create a `.env` file containing:
```
RINKEBY_PKEY="your private key on the rinkeby network"
RINKEBY_RPC_URL="rinkeby url (I used infura)"
MAINNET_PKEY="your mainnet private key (be sure to keep this file locally and set this only when necessary)"
MAINNET_URL="mainnet url (I used infura)"
```
- create something like `metadata.json` for each token you want. Inside there must be a link to the actual artwork of the token
- pin both the metadata file and the artwork file on ipfs. Either use something https://pinata.cloud or ipfs-desktop
- run `truffle compile`

everything should go well, if so you've compiled your contract. Now for deploying it's best to first test it on a test network like rinkeby (only one supported out of the box right now).

- be sure to have set your rinkeby url and private key in the `.env` file before proceeding
- get some test eth from https://www.rinkeby.io/#faucet, use the same address which private key you put in the file
- run `truffle deploy --network rinkeby`

Don't worry if you get some error, check when the transaction completes on etherscna.io (the rinkeby one). It should succeed. 

Now to verify on etherscan (and then mint):
- go to your contract on etherscan.io, then click on "Verify and publish" under the contract tab.
- install `truffle-flattener` from [this repo](https://github.com/fedeb95/truffle-flattener) (main one has an issue currently that breaks with our dependencies)
- run `./flatten.sh`. This creates a file named `etherscan-verify.sol` 
- in etherscan, select single file verification, 0.8.0 as compiler and paste the content of the file generated earlier where it wants the contract source code.
- follow what etherscan says until published
- go again to your contract etherscan page, now you should be able to interact with the contract
- under "write contract" select the `mint` function
- first parameter is the receiving address of the token (usually yours), second one the ipfs hash of the metadata you've pinned earlier with the `ipfs://` prefix
- mint, then visit www.opensea.io/assets/<your contract address>/0 and you should see your freshly minted token!

For mainnet do the same you did for rinkeby, except run `truffle deploy --network mainnet`

I will update this guide to make it better depending on requests, so if there's something you don't get or doesn't work, feel free to open issues or write me!
