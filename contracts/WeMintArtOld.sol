// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

// DEPLOYED WITH 
//                                 **            **                         **
//                                //            /**                        /**
//   ***     **  *****  **********  ** *******  ******     ******   ****** ******
//  //**  * /** **///**//**//**//**/**//**///**///**/     //////** //**//*///**/
//   /** ***/**/******* /** /** /**/** /**  /**  /**       *******  /** /   /**
//   /****/****/**////  /** /** /**/** /**  /**  /**   ** **////**  /**     /**
//   ***/ ///**//****** *** /** /**/** ***  /**  //** /**//********/***     //**
//  ///    ///  ////// ///  //  // // ///   //    //  //  //////// ///       //
//
// :)


contract WeMintArtOld is ERC721URIStorage, ERC721Enumerable, AccessControlEnumerable, Ownable{
    using Counters for Counters.Counter;
    using Strings for uint256;
    Counters.Counter private _tokenIdTracker;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    constructor(string memory name, string memory ticker) ERC721(name, ticker) {
        _setupRole(MINTER_ROLE, _msgSender());
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

     function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override(ERC721,ERC721Enumerable) {
        ERC721Enumerable._beforeTokenTransfer(from, to, tokenId);
    }

    function _burn(uint256 tokenId) internal virtual override(ERC721URIStorage, ERC721) {
        ERC721URIStorage._burn(tokenId);
    }

    function tokenURI(uint256 tokenId) public view virtual 
        override(ERC721URIStorage, ERC721)
        returns (string memory){
        return ERC721URIStorage.tokenURI(tokenId);
    }

    function mint(address to, string memory URI) public virtual {
        require(hasRole(MINTER_ROLE, _msgSender()), "WeMint: must have minter role to mint");

        _mint(to, _tokenIdTracker.current());
        _setTokenURI(_tokenIdTracker.current(), URI);
        _tokenIdTracker.increment();
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721, AccessControlEnumerable, ERC721Enumerable) returns (bool) {
        return ERC721.supportsInterface(interfaceId) || ERC721Enumerable.supportsInterface(interfaceId);
    }

}

